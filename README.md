# grpc_postal

In postal_server one can find all the py packages needed to compile the protobuf
```
poetry install
```

This will install grpcio-tools which will allow you to compile the protobuf in _proto_ folder.

Run

```
python -m grpc_tools.protoc -I proto --python_out=proto --grpc_python_out=proto proto/postal.proto
```

To compile the proto file and output postal_pb2_grpc.py and postal_pb2.py in the same folder. If you want other folder replace in python_out