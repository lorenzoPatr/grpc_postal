from proto.postal_pb2 import AddressRequest
import grpc
from proto.postal_pb2_grpc import AddressParserStub

req = AddressRequest(
  query="AL - KAENHA utara pom bensin glogor carik , ruko no.3b sebelah servis laptop . 80361 Indonesia",
  language="",
  country=""
)

print(req.query)

channel = grpc.insecure_channel("localhost:5001")
client = AddressParserStub(channel)
res = client.parse_address(req)

print("client is running")
for k, v in res.address_components.items():
  # print(k + ": " + res.address_components[k])
  print(k, v)