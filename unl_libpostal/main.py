import os

import grpc

from proto.postal_pb2 import ParseRequest
from proto.postal_pb2_grpc import PostalServiceStub

import google.auth.transport.requests
import google.oauth2.id_token

service_url = "libpostal-sandbox-libpostal--europe-west4-4o6sdqf55q-ez.a.run.app:443"

auth_req = google.auth.transport.requests.Request()
id_token = google.oauth2.id_token.fetch_id_token(auth_req, f"https://libpostal-sandbox-libpostal--europe-west4-4o6sdqf55q-ez.a.run.app")

credentials = grpc.ssl_channel_credentials()
channel = grpc.secure_channel(
    service_url,
    credentials)
client_stub = PostalServiceStub(channel)

req = ParseRequest(
    address="AL - KAENHA, ruko no.3b sebelah servis laptop. 80361 Indonesia"
)
metadata = [("authorization", f"Bearer {id_token}")]

res = client_stub.parse(request=req, metadata=metadata)
print(res)
x = 5

# class Interface:
#     def __init__(self):
#         self._var = 5
#
# class Impl(Interface):
#     def __init__(self):
#         super().__init__()
#         self.__second = self._var
#
#     def prnt(self):
#         print(self.__second)
#
#
# i = Impl()
# i.prnt()