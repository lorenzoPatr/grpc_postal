# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: postal.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x0cpostal.proto\"B\n\x0e\x41\x64\x64ressRequest\x12\r\n\x05query\x18\x01 \x01(\t\x12\x10\n\x08language\x18\x02 \x01(\t\x12\x0f\n\x07\x63ountry\x18\x03 \x01(\t\"\x90\x01\n\x0f\x41\x64\x64ressResponse\x12\x43\n\x12\x61\x64\x64ress_components\x18\x01 \x03(\x0b\x32\'.AddressResponse.AddressComponentsEntry\x1a\x38\n\x16\x41\x64\x64ressComponentsEntry\x12\x0b\n\x03key\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\t:\x02\x38\x01\x32\x43\n\rAddressParser\x12\x32\n\rparse_address\x12\x0f.AddressRequest\x1a\x10.AddressResponseb\x06proto3')



_ADDRESSREQUEST = DESCRIPTOR.message_types_by_name['AddressRequest']
_ADDRESSRESPONSE = DESCRIPTOR.message_types_by_name['AddressResponse']
_ADDRESSRESPONSE_ADDRESSCOMPONENTSENTRY = _ADDRESSRESPONSE.nested_types_by_name['AddressComponentsEntry']
AddressRequest = _reflection.GeneratedProtocolMessageType('AddressRequest', (_message.Message,), {
  'DESCRIPTOR' : _ADDRESSREQUEST,
  '__module__' : 'postal_pb2'
  # @@protoc_insertion_point(class_scope:AddressRequest)
  })
_sym_db.RegisterMessage(AddressRequest)

AddressResponse = _reflection.GeneratedProtocolMessageType('AddressResponse', (_message.Message,), {

  'AddressComponentsEntry' : _reflection.GeneratedProtocolMessageType('AddressComponentsEntry', (_message.Message,), {
    'DESCRIPTOR' : _ADDRESSRESPONSE_ADDRESSCOMPONENTSENTRY,
    '__module__' : 'postal_pb2'
    # @@protoc_insertion_point(class_scope:AddressResponse.AddressComponentsEntry)
    })
  ,
  'DESCRIPTOR' : _ADDRESSRESPONSE,
  '__module__' : 'postal_pb2'
  # @@protoc_insertion_point(class_scope:AddressResponse)
  })
_sym_db.RegisterMessage(AddressResponse)
_sym_db.RegisterMessage(AddressResponse.AddressComponentsEntry)

_ADDRESSPARSER = DESCRIPTOR.services_by_name['AddressParser']
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _ADDRESSRESPONSE_ADDRESSCOMPONENTSENTRY._options = None
  _ADDRESSRESPONSE_ADDRESSCOMPONENTSENTRY._serialized_options = b'8\001'
  _ADDRESSREQUEST._serialized_start=16
  _ADDRESSREQUEST._serialized_end=82
  _ADDRESSRESPONSE._serialized_start=85
  _ADDRESSRESPONSE._serialized_end=229
  _ADDRESSRESPONSE_ADDRESSCOMPONENTSENTRY._serialized_start=173
  _ADDRESSRESPONSE_ADDRESSCOMPONENTSENTRY._serialized_end=229
  _ADDRESSPARSER._serialized_start=231
  _ADDRESSPARSER._serialized_end=298
# @@protoc_insertion_point(module_scope)
