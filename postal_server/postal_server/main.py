from postal_server.proto.postal_pb2 import AddressResponse
import postal_server.proto.postal_pb2_grpc as postal_pb2_grpc
import grpc
from concurrent import futures
from postal.parser import parse_address


class AddressParserServicer(postal_pb2_grpc.AddressParserServicer):
  def parse_address(self, request, context):
    print(request.query)
    entries = parse_address(
        address=request.query,
        language=request.language,
        country=request.country
      )

    res = {k: v for v, k in entries}
    
    return AddressResponse(address_components=res)


def serve():
  print("server started")
  server = grpc.server(futures.ThreadPoolExecutor(max_workers=5))
  postal_pb2_grpc.add_AddressParserServicer_to_server(
    AddressParserServicer(), server
  )

  print("add port")
  server.add_insecure_port("[::]:50051")
  server.start()
  server.wait_for_termination()

if __name__ == "__main__":
  serve()