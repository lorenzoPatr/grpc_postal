from postal.parser import parse_address


def parse(query, language, country):
  return parse_address(query, language='en', country='en')